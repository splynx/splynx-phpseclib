Splynx PhpSecLib add-on
==================

INSTALLATION
------------

### Install via GIT
Install [GIT](https://git-scm.com) then install this project using the following command:

~~~
cd /var/www/splynx/addons
git clone https://bitbucket.org/splynx/splynx-phpseclib.git
cd splynx-phpseclib
composer install
~~~

When you want update add-on run following command:
~~~
git pull
composer update
~~~
