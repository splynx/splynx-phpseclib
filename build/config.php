<?php

$packageDescription = 'Splynx PHP Secure Communications Library add-on';

$controlConfig = [
    'Depends' => 'php8.3-common',
];

// Force version due error with old shit from 2020
// Diff is second number: while here is 3.1.39 - we have 3.0.39 in composer
$currentReleaseVersion = '3.1.43';
